All my config files which gets propogated with a custom script across my computers by class.

I have other big ones which i keep in separate repos : 

* bin directories/script/hacks: https://github.com/chmouel/chmouzies/
* emacs config: https://gitlab.freedesktop.org/chmouel/emacs-config
* zsh config: https://gitlab.freedesktop.org/chmouel/zsh-config
* nvim config: https://gitlab.freedesktop.org/chmouel/nvim-config

other stuff i have made using heavily on my desktop : 

mounch - https://github.com/chmouel/mounch
chmoujump - https://github.com/chmouel/chmoujump

i have another repositoriy for not so secret stuff on my local network managed by yadm https://yadm.io/

all my secrets are actually managed by the excellent password store pass https://www.passwordstore.org/ as a rule if it's in clear it's  not commited

if i use my nixos box, i tend to favour configuring things in home-manager but that's a tedious to follow when doing multiple laptop and not necessay want to install nix everywhere (disk space)


PS: i think this repo was my first ever repo i have ever created on GitHub in 2009 👴🏼😱 
