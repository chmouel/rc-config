#!/usr/bin/env bash
# Copyright 2022 Chmouel Boudjnah <chmouel@chmouel.com>
set -eufo pipefail

running=$(kind get clusters 2>/dev/null)

start() {
    gnome-terminal -- /bin/bash -c "$GOPATH/src/github.com/openshift-pipelines/pipelines-as-code/hack/dev/kind/install.sh;read -p 'Press key to exit' -n1"
    running=yes
}

if [[ ${1:-""} == "click" ]];then
    if [[ -n ${running} ]];then
        gnome-terminal -- /bin/bash -c "${HOME}/.local/bin/stopkind;read -p 'Press key to exit' -n1"
        exit
    else
        start
    fi
elif [[ ${1:-""} == "reinstall" ]];then
    start
fi

if [[ -n ${running} ]];then
    echo "<span foreground=\"cyan\">K </span>"
fi
