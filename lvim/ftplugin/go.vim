setlocal tabstop=2
setlocal shiftwidth=2
set indentexpr=nvim_treesitter#indent()
setlocal autoread

nnoremap <buffer> <leader>lf <CMD>!goimports -w %<CR>
