--- plugins
lvim.plugins = {
    'DNLHC/glance.nvim',
    'folke/trouble.nvim',
    'ggandor/lightspeed.nvim',
    'junegunn/vim-easy-align',
    'kosayoda/nvim-lightbulb',
    'matze/vim-move',
    'michaeljsmith/vim-indent-object',
    'nacro90/numb.nvim',
    'norcalli/nvim-colorizer.lua',
    'ntpeters/vim-better-whitespace',
    'ojroques/nvim-osc52',
    'p00f/nvim-ts-rainbow',
    'ruanyl/vim-gh-line',
    'tpope/vim-unimpaired',
    'tpope/vim-repeat',
    'tpope/vim-surround',
    'windwp/nvim-ts-autotag',
    { 'weilbith/nvim-code-action-menu', cmd = 'CodeActionMenu' },
    { 'saecki/crates.nvim', requires = { 'nvim-lua/plenary.nvim' } },
    { 'sindrets/diffview.nvim', requires = 'nvim-lua/plenary.nvim' },
}

lvim.transparent_window                             = true
lvim.builtin.alpha.active                           = true
lvim.builtin.alpha.mode                             = "startify"
lvim.builtin.terminal.active                        = true
lvim.builtin.nvimtree.setup.renderer.icons.show.git = true
lvim.builtin.treesitter.highlight.enabled           = true
lvim.builtin.treesitter.rainbow.enable              = true
lvim.builtin.gitsigns.opts.yadm.enable              = true
lvim.builtin.treesitter.rainbow.enable              = true
lvim.builtin.autopairs.active                       = false
lvim.builtin.project.active                         = false

lvim.log.level = "warn"
lvim.format_on_save = {
    enabled = true,
    pattern = "*.{go,lua,py,rs}",
    timeout = 1000,
}

lvim.builtin.treesitter.ensure_installed = {
    "bash", "c", "javascript", "json", "lua", "go", "rust",
    "python", "typescript", "tsx", "css", "rust", "java", "yaml", "html",
}
lvim.builtin.treesitter.ignore_install = { "haskell" }
vim.api.nvim_create_autocmd("FileType", {
    pattern = "zsh",
    callback = function()
        -- let treesitter use bash highlight for zsh files as well
        require("nvim-treesitter.highlight").attach(0, "bash")
    end,
})

--- neovim setting
local opt  = vim.opt -- global/buffer/windows-scoped options
local map  = vim.api.nvim_set_keymap
local g    = vim.g
local opts = { noremap = true, silent = true }

opt.autochdir      = true
opt.number         = false
opt.relativenumber = false
opt.showmatch      = true -- highlight matching parenthesis
opt.incsearch      = true
opt.swapfile       = true -- don't use swapfile
opt.hlsearch       = false
opt.smartcase      = true
opt.inccommand     = "split"
opt.termguicolors  = true -- enable 24-bit RGB colors
opt.laststatus     = 2
opt.expandtab      = true -- use spaces instead of tabs
opt.shiftwidth     = 4 -- shift 4 spaces when tab
opt.tabstop        = 4 -- 1 tab == 4 spaces
opt.softtabstop    = 4
opt.smartindent    = true -- autoindent new lines
opt.cursorline     = true
opt.clipboard      = ""
opt.scrolloff      = 8
opt.mouse          = ""
opt.cmdheight      = 0

--- builtin keymap
lvim.keys.visual_mode["<return>"] = 'y'
if vim.env.SSH_AUTH_SOCK == nil then
    lvim.keys.visual_mode["<leader>c"] = '"+y'
    lvim.keys.normal_mode["<leader>v"] = '"+g'
else
    lvim.keys.visual_mode["<leader>c"] = require('osc52').copy_visual
end

lvim.keys.normal_mode["0"] = "^"
lvim.keys.normal_mode["$"] = "g_"
lvim.keys.normal_mode["<C-l>"] = "<C-^>"
lvim.keys.normal_mode["<C-d>"] = "<C-d>zz"
lvim.keys.normal_mode["<C-u>"] = "<C-u>zz"
lvim.keys.normal_mode["gp"] = "`[v`]" --- https://vim.fandom.com/wiki/Selecting_your_pasted_text

vim.cmd([[
cnoremap <C-A> <Home>
cnoremap <C-E> <End>
cnoremap <C-B> <Left>
cnoremap <C-F> <Right>
]])

-- other bindings
lvim.keys.insert_mode["jj"]         = "<ESC><CMD>w<CR>"
lvim.keys.insert_mode["<M-f>"]      = "<C-o>e"
lvim.keys.insert_mode["<M-b>"]      = "<C-o>b"
lvim.keys.insert_mode["<C-f>"]      = "<C-o>l"
lvim.keys.insert_mode["<C-b>"]      = "<C-o>h"
lvim.keys.insert_mode["<C-a>"]      = "<C-o>^"
lvim.keys.insert_mode["<C-e>"]      = "<C-o>$"
lvim.keys.normal_mode["<leader>tc"] = "<cmd>set clipboard+=unnamedplus<CR>"
lvim.keys.normal_mode["<leader>tC"] = "<cmd>set clipboard=<CR>"
lvim.keys.normal_mode["<leader>tw"] = "<cmd>set wrap<CR>"
lvim.keys.normal_mode["<leader>tW"] = "<cmd>set nowrap<CR>"

--- LSP
vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "gopls" })

--- config for formatters
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
    { exe = "black", filetypes = { "python" } },
    { exe = "goimports", filetypes = { "go" } },
    { exe = "gofumpt", filetypes = { "go" } },
    { exe = "prettier", filetypes = { "css", "js", "yaml", "html" } },
    { exe = "shfmt", filetypes = { "sh" } },
}

local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
    { exe = "pylint", filetypes = { "python" } },
    { exe = "golangci_lint", filetypes = { "go" } },
}

-- Telescope
map(
    "n",
    "\\f",
    [[<cmd>lua require('telescope.builtin').git_files({previewer = false})<CR>]],
    opts
)
lvim.keys.normal_mode["[e"] = vim.diagnostic.goto_prev
lvim.keys.normal_mode["]e"] = vim.diagnostic.goto_next

lvim.keys.normal_mode["n"] = "nzzzv"
lvim.keys.visual_mode["\\s"] = "\"zy<cmd>exec 'Telescope grep_string default_text=' . escape(@z, ' ')<cr>"
lvim.keys.normal_mode["<leader>sS"] = " cmd>Telescope grep_string<cr>"
lvim.keys.normal_mode["<leader>ss"] = '<cmd>luado require("telescope.builtin").grep_string({prompt_title = "Find in Git Project", cwd = vim.fn.fnamemodify(vim.fn.finddir(".git", ".;"), ":h")})<cr>'
lvim.keys.normal_mode["<leader>sG"] = "<cmd>Telescope live_grep<cr>"
lvim.keys.normal_mode["<leader>sg"] = '<cmd>luado require("telescope.builtin").live_grep({prompt_title = "Find in Git Project", cwd = vim.fn.fnamemodify(vim.fn.finddir(".git", ".;"), ":h")})<cr>'
lvim.keys.normal_mode["<leader><leader>"] = "<cmd>Telescope current_buffer_fuzzy_find<cr>"
lvim.keys.normal_mode["<leader>s'"] = "<cmd>Telescope resume<cr>"
lvim.keys.normal_mode["<leader>lt"] = "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>"
lvim.keys.normal_mode["<leader>se"] = "<cmd>Telescope symbols<CR>"

--- Buffer
lvim.keys.normal_mode["<S-TAB>"]   = "<cmd>BufferLineCyclePrev<cr>"
lvim.keys.normal_mode["<TAB>"]     = "<cmd>BufferLineCycleNext<cr>"
lvim.keys.normal_mode["<leader>1"] = "<cmd>BufferLineGoToBuffer 1<cr>"
lvim.keys.normal_mode["<leader>2"] = "<cmd>BufferLineGoToBuffer 2<cr>"
lvim.keys.normal_mode["<leader>3"] = "<cmd>BufferLineGoToBuffer 3<cr>"
lvim.keys.normal_mode["<leader>4"] = "<cmd>BufferLineGoToBuffer 4<cr>"
lvim.keys.normal_mode["<leader>5"] = "<cmd>BufferLineGoToBuffer 5<cr>"
lvim.keys.normal_mode["<leader>6"] = "<cmd>BufferLineGoToBuffer 6<cr>"
lvim.keys.normal_mode["<leader>7"] = "<cmd>BufferLineGoToBuffer 7<cr>"
lvim.keys.normal_mode["<leader>8"] = "<cmd>BufferLineGoToBuffer 8<cr>"
lvim.keys.normal_mode["<leader>9"] = "<cmd>BufferLineGoToBuffer 9<cr>"

--- crates
require("crates").setup()

-- diffview
require("diffview").setup({})

--- lightbulb / lsp actions
require('nvim-lightbulb').setup({ autocmd = { enabled = true } })

--- troubles
require("trouble").setup({})
lvim.keys.normal_mode["\\t"] = "<cmd>TroubleToggle<cr>"

--- CodeactionMenu
lvim.keys.normal_mode["\\a"] = "<cmd>CodeActionMenu<cr>"

--- better whitespace
g.better_whitespace_enabled = 1
g.strip_only_modified_lines = 1
g.strip_whitespace_confirm  = 0
vim.api.nvim_exec([[
  autocmd TermEnter * DisableWhitespace
]], false)
lvim.keys.normal_mode["<leader>S"] = "<cmd>StripWhitespace<cr>"

---
require("numb").setup {
    show_numbers    = true,
    show_cursorline = true,
}

---
require("colorizer").setup({ "css", "scss", "html", "javascript" }, {
    RGB      = true, -- #RGB hex codes
    RRGGBB   = true, -- #RRGGBB hex codes
    RRGGBBAA = true, -- #RRGGBBAA hex codes
    rgb_fn   = true, -- CSS rgb() and rgba() functions
    hsl_fn   = true, -- CSS hsl() and hsla() functions
    css      = true, -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
    css_fn   = true, -- Enable all CSS *functions*: rgb_fn, hsl_fn
})

---
require("nvim-ts-autotag").setup()

-- easy align
vim.api.nvim_exec([[
    xmap ga <Plug>(EasyAlign)
    nmap ga <Plug>(EasyAlign)
]], false)

---
require('lightspeed').setup({
    jump_to_unique_chars = false,
    safe_labels = {}
})

---
require('glance').setup({})
lvim.keys.normal_mode["\\r"] = "<CMD>Glance references<CR>"
