(setq! user-full-name "Chmouel Boudjnah"
       user-mail-address "chmouel@chmouel.com"
       enable-local-variables :all ;; Is it a good idea ?? probably not :\
       display-line-numbers-type nil
       confirm-kill-emacs nil
       org-directory "~/Sync/orgs"
       doom-scratch-initial-major-mode 'emacs-lisp-mode
       disabled-command-function nil)


(cond
 ((SYS "kodi")
  (setenv "SSH_AUTH_SOCK" (expand-file-name "~/.ssh/ssh_auth_sock"))
  (setq! doom-theme 'doom-opera))
 ((SYS "pignon")
  (setq!
   doom-theme 'kaolin-ocean
   doom-variable-pitch-font (font-spec :family "JetBrains Mono NL" :size 16)
   doom-font (font-spec :family "JetBrains Mono NL" :size 23 :weight 'semi-light)))
 ((SYS "macbook")
  (setq!
   doom-theme 'kaolin-aurora
   doom-variable-pitch-font (font-spec :family "JetBrains Mono" :size 13)
   doom-font (font-spec :family "JetBrains Mono NL" :size 19 :weight 'semi-light))
  (setq! ns-right-alternate-modifier 'left)))

(add-to-list 'initial-frame-alist '(fullscreen . maximized))
(remove-hook 'doom-first-buffer-hook #'smartparens-global-mode)

;; Save automatically the buffer when existing the insert mode.  make sure it's
;; a buffer attached to a file-name if we are exiting from vc git commit mode
;; then commit it
;; key-chord-mode need to be after
(key-chord-define
 evil-insert-state-map  "jj"
 (cmd!
  (evil-normal-state)
  (cond ((eq major-mode 'vc-git-log-edit-mode)
         (log-edit-done))
        (buffer-file-name (save-buffer)))))
(after! key-chord (key-chord-mode +1))

(after! windmove
  (windmove-default-keybindings))

(use-package! emacs
  :config
  (map!
   (:map view-mode-map :gin "q" #'View-quit)
   "M-w"   #'easy-kill
   "C-1"   #'delete-other-windows
   "C-2"   #'split-window-vertically
   "<f1>"  nil
   "<f2>"  nil
   "<f3>"  nil
   "<f4>"  nil
   "C-l" (cmd! (switch-to-buffer (other-buffer (current-buffer) 1)))
   "C-x C-k" (cmd! (kill-buffer (current-buffer))))
  ;; Create or Switch to tab with config.el
  ;; need to extract to its own function
  (defun my-switch-focus-tab(name arg)
    (let ((tab-bar-new-tab-choice arg))
      (if (tab-bar--tab-index-by-name name)
          (tab-bar-switch-to-tab name)
        (tab-new))))
  (defun my-switch-focus-file(filename)
    (my-switch-focus-tab
     (file-name-nondirectory filename)
     (cmd! (find-file filename))))
  (map!
   (:leader
    :prefix-map ("f" . "file")
    :desc "Find file others" "o" #'ff-find-other-file
    :desc "Open config.el quickly" "c"
    (cmd!
     (my-switch-focus-file (concat doom-user-dir "config.el"))))))

(after! evil
  (map!
   :leader
   :desc "window" "u"  evil-window-map
   :desc "Search Buffer" "SPC" #'+default/search-buffer
   :desc "Save Buffer" "w" #'save-buffer)
  (map!
   :map evil-window-map
   "SPC" #'rotate-layout
   "C-SPC" #'rotate-window)
  (map!
   :leader
   (:leader :prefix-map ("g" . "git")
    :desc "VC diff" "d" #'vc-diff
    :desc "VC next action" "v" #'vc-next-action))
  (map!
   :i "C-y" #'evil-paste-after-cursor-after
   :n "q" nil
   :n "0" #'evil-first-non-blank
   :n "-" #'evil-end-of-line-or-visual-line)
  (map! :map evil-window-map
        "o" #'doom/window-maximize-buffer))

(after! isearch
  (map! :map isearch-mode-map
        "C-d"  #'avy-isearch
        "C-o"  #'isearch-occur))

(use-package! vterm
  :config
  (evil-set-initial-state 'vterm-mode 'emacs)
  (defun my-switch-to-vterm()
    (interactive)
    (my-switch-focus-tab "*vterm*"
                         (cmd! (if (get-buffer "*vterm*")
                                   (switch-to-buffer "*vterm*")
                                 (call-interactively '+vterm/here)))))
  (map!
   :n "C-t" #'my-switch-to-vterm
   (:leader
    :prefix-map ("o" . "open")
    :desc "Vterm Toggle" "T" #'+vterm/toggle
    :desc "Open vterm in a tab" "t"
    (cmd!
     (my-switch-focus-tab "*vterm*" #'my-switch-to-vterm)))))

(after! tab-bar
  (map!
   :leader
   :desc "First tab" "1" (cmd! (tab-bar-select-tab 1))
   :desc "Second tab" "2" (cmd! (tab-bar-select-tab 2))
   :desc "Third tab" "3" (cmd! (tab-bar-select-tab 3))
   :desc "Fourth tab" "4" (cmd! (tab-bar-select-tab 4))
   :desc "Fifth tab" "5" (cmd! (tab-bar-select-tab 5)))
  (map!
   (:leader :prefix-map ("t" . "toggle")
    :desc "Browse project in tab" "p" (cmd! (let ((tab-bar-new-tab-choice 'projectile-switch-project)) (tab-new)))
    :desc "New tab" "n" 'tab-bar-new-tab
    :desc "Close tab" "k" 'tab-bar-close-tab)
   (:when IS-MAC
     :n "s-{" (cmd! (tab-move -1))
     :n "s-}" #'tab-move
     :n "s-[" #'tab-previous
     :n "s-]" #'tab-next)
   (:unless (SYS "kodi")
     :n [C-backtab] #'tab-previous
     :n [C-tab] #'tab-next))
  (setq!
   tab-bar-new-button-show 'nil
   tab-bar-close-button nil
   tab-bar-history-mode 'nil
   tab-bar-new-tab-to 'rightmost
   tab-bar-tab-hints t
   ;; tab-line-close-button-show nil
   ;; tab-line-new-button-show nil
   tab-bar-new-tab-choice 'dired-jump)
  (when IS-MAC
    (setq tab-bar-select-tab-modifiers '(super))
    (setq tab-bar-select-tab-modifiers '(meta)))
  (custom-set-faces!
    '(tab-bar-tab :inherit font-lock-builtin-face :foreground nil :weight bold :background nil))
  (tab-bar-mode +1))

(after! consult
  (setq! consult-narrow-key "<")
  (map!
   "C-\\" #'consult-buffer))

(after! expand-region
  (map! :n "+" #'er/expand-region))

(after! rg
  (rg-define-search my-rg-project
    :query ask
    :format regexp
    :files "all"
    :dir project))

(after! helpful (evil-set-initial-state 'helpful-mode 'emacs))

(use-package! projectile
  :config
  (map!
   :n "C-f" #'projectile-find-file)
  (setq! projectile-switch-project-action 'dired-jump))

(after! dired-x
  (add-hook! 'dired-mode dired-omit-mode)
  (setq!   dired-omit-files
           (concat "^\\.\\|^\\.?#\\|^\\.$\\|^\\.\\.$\\|"
                   "^Thumbs.db$\\|^TAGS$\\|^tags$\\|__pycache__$"))
  (map! :map dired-mode-map "s" #'dired-up-directory))

(use-package! diredfl
  :hook (dired-mode . diredfl-mode))

(use-package! wgrep
  :config
  (map! :map dired-mode-map "e" #'wdired-change-to-wdired-mode))

(use-package! go-mode
  :config
  (map!
   :map go-mode-map
   (:leader
    (:prefix-map ("l" . "local")
     :desc "Run file test" "f" #'go-test-current-file
     :desc "Run current function test or subtest" "t" #'my-gotest-maybe-ts-run
     :desc "Compile or Recompile compilation or others" "r" #'my-recompile
     (:prefix ("d" . "debug")
      :desc "Toggle Breakpoint" "t" #'dap-breakpoint-toggle
      :desc "Hydra Dap" "h" #'dap-hydra
      :desc "Last" "l" #'dap-debug-last
      :desc "Debug recent" "r" #'dap-debug-recent
      :desc "Debug current function" "d" #'dap-debug))))
  (setq! gofmt-command "goimports")
  (add-hook! 'before-save-hook #'gofmt-before-save)
  (add-hook! 'go-mode-hook (cmd! (setq-local go-run-args "-v"))))

(use-package! tree-sitter
  :hook
  (shell-script-mode . tree-sitter-hl-mode)
  (python-mode . tree-sitter-hl-mode)
  (rustic-mode . tree-sitter-hl-mode)
  (go-mode . tree-sitter-hl-mode))

(use-package! gotest
  :commands (my-gotest-maybe-ts-run go-test--get-current-test-info)
  :after go-mode
  :custom
  (go-test-verbose t)
  :hook
  (go-mode . (lambda ()(interactive) (setq go-run-args "-v")))
  :config
  (defun my-go-test-current-project()
    (interactive)
    (let ((default-directory (project-root (project-current t))))
      (go-test-current-project)))
  (defun my-gotest-maybe-ts-run()
    "Run current unless we recognize a subtest then well we run it"
    (interactive)
    (let ((testrunname)
          (gotest (cadr (go-test--get-current-test-info))))
      (save-excursion
        (goto-char (line-beginning-position))
        (re-search-forward "name:[[:blank:]]*\"\\([^\"]*\\)\"" (line-end-position) t))
      (setq testrunname (match-string-no-properties 1))
      (if testrunname (setq gotest (format "%s/%s" gotest (shell-quote-argument testrunname))))
      (go-test--go-test (concat "-run " gotest "\\$ .")))))

(use-package! emacs
  :custom
  (next-error-message-highlight t)
  :config
  (map!
   :map prog-mode-map
   :ngvi "C-<return>" #'my-recompile)
  (map!
   (:leader
    :prefix-map ("l" . "local")
    :desc "Compile or Recompile compilation or others" "r" 'my-recompile))
  (defun my-recompile (args)
    (interactive "P")
    (cond
     ((eq major-mode #'emacs-lisp-mode)
      (call-interactively 'eros-eval-defun))
     ((get-buffer "*compilation*")
      (with-current-buffer"*compilation*"
        (recompile)))
     ((get-buffer "*Go Test*")
      (with-current-buffer "*Go Test*"
        (recompile)))
     ((and (eq major-mode #'go-mode)
           buffer-file-name
           (string-match
            "_test\\'" (file-name-sans-extension buffer-file-name)))
      (my-gotest-maybe-ts-run))
     ((and (get-buffer "*cargo-test*")
           (boundp 'my-rustic-current-test-compile)
           my-rustic-current-test-compile)
      (with-current-buffer "*cargo-test*"
        (rustic-cargo-test-run my-rustic-current-test-compile)))
     ((get-buffer "*cargo-run*")
      (with-current-buffer "*cargo-run*"
        (rustic-cargo-run-rerun)))
     ((get-buffer "*pytest*")
      (with-current-buffer "*pytest*"
        (recompile)))
     ((eq major-mode #'python-mode)
      (compile (concat python-shell-interpreter " " (buffer-file-name))))
     ((call-interactively 'compile)))))

(after! Info-mode
  (map! (:map Info-mode-map "<RET>" #'Info-follow-nearest-node)))

(after! yasnippet
  (map! (:map prog-mode-map
         :i "<backtab>" #'yas-expand-from-trigger-key)))

(after! lsp-mode
  (setq!
   lsp-auto-guess-root t
   lsp-yaml-format-enable t
   lsp-lens-enable nil
   lsp-go-use-gofumpt t
   lsp-enable-file-watchers nil
   lsp-treemacs-error-list-current-project-only t
   lsp-go-analyses
   '((nilfunc . t)
     (lostcancel . t)
     (loopclosure . t)
     (infertypeargs . t)
     (ifaceassert . t)
     (httpresponse . t)
     (errorsas . t)
     (unusedwrite . t)
     (nilness . t)))
  (map!
   (:map lsp-mode-map
    :n   "<f8>" #'lsp-treemacs-errors-list
    :nv  "<f2>" #'lsp-rename)))

(use-package! turbo-log
  :disabled
  :config
  (map!
   (:map prog-mode-map
         "M-p" #'turbo-log-print
         (:leader
          :prefix-map ("l" . "local")
          :desc "Turbo Log" "l" #'turbo-log-print)))
  (setq turbo-log-msg-format-template "\"CHMOUDEBUG: %s\""))

(use-package! dap-mode
  :custom
  (dap-auto-configure-features '(locals tooltip))
  :config
  (map!
   (:map lsp-treemacs-generic-map
         [tab]  #'treemacs-TAB-action
         "<RET>" #'treemacs-RET-action)
   (:map dap-mode-map
         "<f10>" #'dap-next
         "<f5>"  #'dap-continue
         "<f12>" #'dap-hydra
         "<f11>" #'dap-step-in)))

(use-package! magit
  :after-call
  (magit-toplevel magit-process-git magit-get))
(after! prog-mode
  (defun yas--magit-email-or-default ()
    "Get email from GIT or use default"
    (if (and (fboundp 'magit-toplevel)
             (magit-toplevel "."))
        (magit-get "user.email")
      user-mail-address)))

(use-package! company
  :custom
  (company-idle-delay 1))

(use-package! python
  :hook
  (python-mode . tree-sitter-hl-mode)
  :bind (:map python-mode-map ("C-c i" . my-py-import-add))
  :config
  (defun my-py-insert-import (arg import)
    (save-excursion
      (goto-char (point-min))
      (while (or (python-info-current-line-comment-p)
		         (python-info-docstring-p))
	    (forward-line))
      (insert
       (concat
	    "import " import
	    (if arg (concat " as " (read-from-minibuffer "Import as: "))))
       "\n")))

  (defun my-py-import-add (arg import)
    (interactive
     (list
      current-prefix-arg
      (read-from-minibuffer "Package: " )))
    (my-py-insert-import arg import)
    (py-isort-buffer)))

(use-package! go-playground
  :after go-mode
  :config
  (defun my-jump-go-playground-snippet (snippet)
    (interactive
     (list
      (completing-read
       "Snippet: "
       (mapcar
        (lambda (x)
          (f-base x))
        (f-directories go-playground-basedir)))))
    (find-file
     (concat
      go-playground-basedir "/" snippet "/"
      (s-replace-regexp "-at.*" "" snippet) ".go")))
  :custom
  (go-playground-ask-file-name t)
  (go-playground-init-command "go mod init github.com/chmouel/$(basename $PWD|sed 's/-at.*//')")
  (go-playground-basedir "~/Sync/goplay"))
